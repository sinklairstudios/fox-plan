# PwaFoxplan

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 17.0.0.

> For macOS Install the CLI using [Homebrew](brew.sh) `brew install angular-cli`

## Requirements

* Node 21

## Setup

Run `npm install`

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Testing caching and service workers

Install simple http server:
```bash
brew install http-server
```

Serve application as "production":
```bash
ng build --configuration production
cd dist/pwa-foxplan
http-server -p 4200
```

For more information on request caching, please review [this article](https://christianlydemann.com/how-to-cache-http-requests-in-an-angular-pwa/) and [Angular's Service Worker Documentation](https://angular.io/guide/service-worker-config).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
