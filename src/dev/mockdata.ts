import { ConventionSchedule, Panel, Room } from 'src/app/objects/panel';
import CONFIGURATION from '../assets/events/configuration.json';
import { ConventionAbout } from 'src/app/objects/convention';

export const DEV_PLACEHOLDER = {
  name: 'Dev',
  path: 'dev'
};

export function getDevConfig() {
  let newConfig = CONFIGURATION;
  newConfig.conventions.push(DEV_PLACEHOLDER);
  return newConfig;
}


export function getSandboxConvention(): ConventionAbout {
  const about: ConventionAbout = {
    name: "dev",
    title: "Dev Test",
    hashtag: "#foxplan_dev",
    version: 1,
    time_zone: "UTC-8",
    time_zone_city: "America/Los_Angeles",
    style: {
      logo_name: "logo.png",
      background_image_name: "images/background.jpg",
      background_color: "#031825",

      primary_font: "Helvetica",
      primary_font_color: "#031825"   ,
      primary_color: "#e1a537",

      secondary_font_color: "#FFFFFF",
      secondary_color: "#000000"
    },
    maps: [],
    faqs: []
  }
  return about;
}

function generateDateArray(): Date[] {
  const now = new Date();
  const dates: Date[] = [];
  const numberOfDates = 20
  const intervalSizeInHours = 4

  // Calculate the start time (yesterday same time)
  const startTime = new Date(now.getTime() - 48 * 60 * 60 * 1000);

  // Generate dates, each 6 hours apart
  for (let i = 0; i < numberOfDates; i++) {
    const date = new Date(startTime.getTime() + i * intervalSizeInHours * 60 * 60 * 1000);
    dates.push(date);
  }

  return dates;
}

export function getSandboxSchedule(): ConventionSchedule {
  const schedule: Panel[] = []
  const dates = generateDateArray();
  const offset = '-08:00';

  for (let i = 0; i < dates.length; i++) {
    const date = dates[i];
    const startDate = date.toISOString().replace('Z', '') + offset;
    const endTimeOffset =  6 * 60 * 60 * 1000; // 6h, 60 seconds, 60 minutes, 1000 ms
    const endDate = new Date(date.getTime() + endTimeOffset).toISOString().replace('Z', '') + offset;

    const location: Room = new Room();
    location.name = "Location " + i;

    const panel: Panel = {
      id: ""+i,
      name: "Panel " + i,
      startDate: startDate,
      endDate: endDate,
      description: "Description panel " + i,
      presenters: [],
      type: "panel",
      location: location
    }

    schedule.push(panel);
  }

  return {
    name: "dev",
    entries: schedule
  }
}
