import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PanelsComponent } from './panels.component';
import { MyOnlyBookmarkedPipe } from '../pipes/onlyBookmarked';
import { SearchPipe } from '../pipes/search';

import { RouterTestingModule } from "@angular/router/testing";
import { MatListModule } from '@angular/material/list';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule } from '@angular/forms';



describe('PanelsComponent', () => {
  let component: PanelsComponent;
  let fixture: ComponentFixture<PanelsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
    imports: [
        RouterTestingModule,
        MatListModule,
        MatInputModule,
        MatFormFieldModule,
        FormsModule,
        PanelsComponent,
        MyOnlyBookmarkedPipe,
        SearchPipe
    ]
})
    .compileComponents();

    fixture = TestBed.createComponent(PanelsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
