import { ActivatedRoute, Router, RouterLink } from '@angular/router';
import { Component, OnInit, isDevMode, OnDestroy, inject } from '@angular/core';
import { ViewportScroller, Location, NgClass, DatePipe } from '@angular/common';

import { Panel } from '../objects/panel';

import { PanelService } from '../services/panel.service';
import { SearchService } from '../services/search.service';

import { ConventionService } from '../services/convention.service';
import { InstallPromptComponent } from '../install-prompt/install-prompt.component';
import { MatNavList, MatListSubheaderCssMatStyler, MatListItem, MatListItemTitle, MatListItemLine, MatListItemMeta } from '@angular/material/list';
import { MatIconButton } from '@angular/material/button';
import { MatIcon } from '@angular/material/icon';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ContextMenuComponent } from '../context-menu/context-menu.component';
import { MyOnlyBookmarkedPipe } from '../pipes/onlyBookmarked';
import { SearchPipe } from '../pipes/search';
import { areArraysEqual } from '../utils/equality';

@Component({
    selector: 'app-panels',
    templateUrl: './panels.component.html',
    styleUrls: ['./panels.component.css'],
    imports: [InstallPromptComponent, MatNavList, MatListSubheaderCssMatStyler, MatIconButton, MatIcon, MatListItem, NgClass, RouterLink, MatListItemTitle, MatListItemLine, MatListItemMeta, ContextMenuComponent, DatePipe, MyOnlyBookmarkedPipe, SearchPipe]
})
export class PanelsComponent implements OnInit, OnDestroy {
  private panelService = inject(PanelService);
  private router = inject(Router);
  private route = inject(ActivatedRoute);
  private location = inject(Location);
  private viewportScroller = inject(ViewportScroller);
  private searchService = inject(SearchService);
  private conventionService = inject(ConventionService);

  panelsByDay: Map<string, Array<Panel>> = new Map();
  days: string[] = [];
  query: string = '';
  about: any = {};
  panels: Panel[] = [];
  lastUpdatedAt: Date | null = null;
  useConventionTime?: boolean;

  // periodically check to update UI
  private intervalChecking: any;

  constructor(
    private _snackBar: MatSnackBar
  ) {
    // subscribe to data updates
    this.conventionService.about.subscribe((value: any) => {
      this.about = value
      this.useConventionTime = this.conventionService.useConventionTime()
    });
    this.panelService.panels.subscribe(value => {
      if (!areArraysEqual(value, this.panels) && this.panels.length > 0) {        
        this.showScheduleHasUpdate()
      }
      this.panels = value
    });
    this.panelService.panelsByDay.subscribe(value => this.panelsByDay = value);
    this.panelService.days.subscribe(value => this.days = value);
    this.panelService.lastUpdatedAt.subscribe(value => this.lastUpdatedAt = value);

    // subscribe to search field updates
    this.searchService.query$.subscribe(query => this.query = query);
  }

  showScheduleHasUpdate() {
    const message = 'Schedule Updated';
    const action = 'Ok';
    this._snackBar.open(message, action, { duration: 5000 });
  }

  isLive(panel: Panel): boolean {
    const now = new Date();
    return (new Date(panel.startDate) <= now && now < new Date(panel.endDate))
  }

  isAboutToStart(panel: Panel): boolean {
    const now = new Date();
    const startTime = new Date(panel.startDate);
    const threshold = 15; // minutes before start
    return now < startTime && startTime <= new Date(now.getTime() + threshold * 60_000);
  }

  hasPassed(panel: Panel): boolean {
    const now = new Date();
    return now > new Date(panel.endDate);
  }

  ngOnInit() {
    const url = this.router.url
    const containsAnchor = url.includes('#');

    var path = this.location.path();

    if (!containsAnchor && !path.startsWith("/schedule/")) {
      this.scrollToNow()
    }

    const intervalInSeconds = 10;
    this.intervalChecking = setInterval(() => {
      // This will trigger change detection each interval
      // this is needed for updating the live status
    }, intervalInSeconds * 1000); // Check every second, adjust as needed
  }

  scrollToNow() {
    let today = new Date();
    let formattedDayString = this.panelService.getFormattedDayString(today);

    this.viewportScroller.scrollToAnchor(formattedDayString);
    if(isDevMode()) {
      console.log("going to date: " + formattedDayString);
    }

    this.router.navigate([], {
      fragment: formattedDayString,
      queryParamsHandling: 'merge',
    });
  }

  goToDay(formattedDayString: string, smooth: boolean = false): void {
    const element = document.getElementById(formattedDayString);
    if (element) {
      element.scrollIntoView({
        behavior: smooth ? 'smooth' : 'instant'
      });
    }
  }

  ngOnDestroy(): void {
    if (this.intervalChecking) {
      clearInterval(this.intervalChecking);
    }
  }

  ///////
  // Jump to previously selected panel
  ngAfterViewInit() {
    var id = this.route.snapshot.paramMap.get('id');
    if (id) {
      this.sleep(200).then(() => {
        this.scrollToAnchoringPosition(this.getSanitizedId(id || ""));
      })
    }
  }

  private async sleep(ms: number) {
    await this._sleep(ms);
  }

  private _sleep(ms: number) {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }

  public getSanitizedId(text: string): string {
    return "p"+text;
  }

  public scrollToAnchoringPosition(elementId: string): void {
    this.viewportScroller.scrollToAnchor(elementId);
  }

  selectedPanel?: Panel;
  onSelect(panel: Panel): void {
    // // this.viewportScroller.scrollToAnchor("#Sat Jan 18 2020 00:00:00 GMT-0700 (Mountain Standard Time)");
    // // this.router.navigate([], { fragment: panel.id });
    this.selectedPanel = panel;
  }

  // Context Menu
  showContextMenu = false;
  contextMenuPanel?: Panel;
  contextMenuX = 0;
  contextMenuY = 0;

  // set context menu trigger for desktop
  onRightClick(event: MouseEvent, panel: Panel) {
    event.preventDefault();
    this.openContextMenu(event, panel)
  }

  // set context menu trigger for mobile
  longPressTimeout: any = null;
  onTouchStart(event: TouchEvent, panel: Panel) {
    // event.preventDefault();
    this.longPressTimeout = setTimeout(() => {
      this.openContextMenu(event, panel);
    }, 1000); // Long press threshold (e.g., 1000 milliseconds)
  }

  onTouchEnd(event: TouchEvent) {
    if (this.longPressTimeout) {
      clearTimeout(this.longPressTimeout);
      this.longPressTimeout = null; // Reset the timeout variable
    }
  }

  openContextMenu(event: TouchEvent | MouseEvent, panel: Panel) {
    this.contextMenuX = (event instanceof MouseEvent) ? event.clientX : event.touches[0].clientX;
    this.contextMenuY = (event instanceof MouseEvent) ? event.clientY : event.touches[0].clientY;
    // manually move context menu so the top left corner is aligned with cursor
    // this is done so on mobile the context menu is more noticable as it otherwise
    // might be displayed underneath the users finger
    this.contextMenuY -= 64;
    this.contextMenuPanel = panel;

    this.showContextMenu = true;
  }

  closeContextMenu() {
    // Logic to hide the context menu
    this.showContextMenu = false;
  }
}
