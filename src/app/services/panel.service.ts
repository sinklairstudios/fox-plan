import { Injectable, isDevMode, inject } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { Panel, Room, Presenter, Social, ConventionSchedule } from '../objects/panel';

import { ConventionService } from './convention.service';
import { BookmarkService } from './bookmark.service';
import { ConventionAbout } from '../objects/convention';

@Injectable({
  providedIn: 'root'
})
export class PanelService {
  private conventionService = inject(ConventionService);
  private bookmarkService = inject(BookmarkService);


  about: ConventionAbout | null = null;
  schedule: ConventionSchedule | null = null;
  lastUpdatedAt = new BehaviorSubject<Date | null>(null);
  panels = new BehaviorSubject<Panel[]>([]) ;
  panelsByDay = new BehaviorSubject<Map<string, Array<Panel>>>(new Map());
  days= new BehaviorSubject<string[]>([]);

  constructor() {
    this.conventionService.about.subscribe((newAbout) => this.about = newAbout);
    this.conventionService.schedule.subscribe((newSchedule) => {
      // guard if schedule empty
      if (!newSchedule) {
        this.panels.next(new Array());
        this.lastUpdatedAt.next(new Date())
        return;
      }

      this.schedule = newSchedule;

      const lastUpdatedAt = newSchedule?.lastUpdatedAt ? new Date(newSchedule.lastUpdatedAt) : this.lastUpdatedAt.value
      
      var newPanels = newSchedule.entries;
      newPanels = this.loadBookmarks(newPanels);
      newPanels = this.fillSearchTerms(newPanels);
      
      if(isDevMode()) {
        console.log("New Panels:")
        console.log(newPanels);
      }
      
      this.panels.next(newPanels);
      this.lastUpdatedAt.next(lastUpdatedAt)
      this.sortIntoDays(newPanels);
    });
  }

  public getFormattedDayString(date: Date) {
    if (this.conventionService.useConventionTime() && this.about) {
      // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl/DateTimeFormat
      let options: Intl.DateTimeFormatOptions = { timeZone: this.about.time_zone_city, weekday: "long", month: "short", day: "numeric" };
      return new Intl.DateTimeFormat('en-US', options).format(date);
    } else {
      let options: Intl.DateTimeFormatOptions = { weekday: "long", month: "short", day: "numeric" };
      return new Intl.DateTimeFormat('en-US', options).format(date);
    }
  }

  sortIntoDays(panels: Panel[]): void {
    if (panels === undefined || panels.length < 1) { return; }

    var panelByDay: Map<string, Array<Panel>> = new Map();

    if(isDevMode()) {
      console.log(panels);
    }

    // sort array by date
    panels = panels.sort((a,b) => {
      return (a.startDate < b.startDate) ? -1 : 1;
    });

    // split array up in days
    for (let panelIndex = 0; panelIndex < panels.length; panelIndex++) {
      let panel = panels[panelIndex];
      let startDate: Date = new Date(Date.parse(panel.startDate));
      if (this.isValidDate(startDate)) {
        var dateString = "";
        dateString = this.getFormattedDayString(startDate);

        if(isDevMode()) {
          console.log(dateString);
        }

        if (!panelByDay.has(dateString)) {
          panelByDay.set(dateString, []);
        }
        panelByDay.get(dateString)?.push(panel);
      }
    }

    this.panelsByDay.next(panelByDay);
    this.days.next(Array.from(panelByDay.keys()));
  }

  isValidDate(date: Date): boolean {
    return date instanceof Date;
  }

  fillSearchTerms(panels: Panel[]): Panel[] {
    for (let index = 0; index < panels.length; index++) {
      var searchterms: string = "";
      searchterms += panels[index].id;
      searchterms += panels[index].name;
      searchterms += panels[index].description;
      searchterms += panels[index].type;
      searchterms += panels[index].location?.name;
      searchterms += panels[index].location?.building;
      searchterms += panels[index].presenters?.map(presenter => presenter.name).join(",");
      panels[index].searchterms = searchterms;
    }
    return panels;
  }

  private loadBookmarks(panels: Panel[]): Panel[] {
    if (!this.about) {
      console.log("about not loaded");
      return panels;
    }

    for (let index = 0; index < panels.length; index++) {
      panels[index].isBookmarked = this.bookmarkService.isBookmarked(this.about.name, panels[index].id);
    }
    return panels;
  }
}
