import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  private lastValue: string = "";
  private _query: BehaviorSubject<string>;
  query$: Observable<string>;
  
  constructor() { 
    this._query = new BehaviorSubject('') as BehaviorSubject<string>;
    this.query$ = this._query.asObservable();
  }

  get query() {
    return this.lastValue;
  }
  
  set query(query: string) {
    this.lastValue = query;
    this._query.next(query);
  }
}
