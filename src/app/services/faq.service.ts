import { Injectable, inject } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { FAQ } from '../objects/faq';

import { ConventionService } from './convention.service';
import { ConventionAbout } from '../objects/convention';

@Injectable({
  providedIn: 'root'
})
export class FAQService {
  private conventionService = inject(ConventionService);


  faqs = new BehaviorSubject<FAQ[]>([]);

  constructor() {
    this.conventionService.about.subscribe((about: ConventionAbout | null) => {
      const newFaqs = about?.faqs ?? [];
      this.faqs.next(this.fillSearchTerms(newFaqs));
    });
  }

  fillSearchTerms(faqs: FAQ[]): FAQ[] {
    for (var index in faqs) {
        var searchterms: string = "";
        searchterms += faqs[index].name;
        searchterms += faqs[index].description;
        faqs[index].searchterms = searchterms;
    }
    return faqs;
  }
}
