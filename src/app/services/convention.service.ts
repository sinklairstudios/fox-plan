import { Injectable, isDevMode, inject } from '@angular/core';
import { BehaviorSubject, firstValueFrom } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';

import CONFIGURATION from '../../assets/events/configuration.json';
import { ConventionAbout } from '../objects/convention';
import { ConventionSchedule, Panel, Room } from '../objects/panel';
import { getDevConfig, getSandboxConvention, getSandboxSchedule } from 'src/dev/mockdata';

@Injectable({
  providedIn: 'root'
})
export class ConventionService {
  private httpClient = inject(HttpClient);
  private route = inject(ActivatedRoute);
  private router = inject(Router);


  config = CONFIGURATION;
  currentConvention: string = this.config.default;
  about = new BehaviorSubject<ConventionAbout | null>(null);
  schedule = new BehaviorSubject<ConventionSchedule | null>(null);

  constructor() {
    this.init();
  }

  async init () {
    if (isDevMode()) {
      this.config = getDevConfig();
    }
    await this.getConventionFlag()
    await this.loadAbout();
  }

  ngOnDestroy() {
    if (this.updateInterval) {
      clearInterval(this.updateInterval);
    }
  }


  // TODO: expose this on the settings page
  shouldUseConventionTime = true;

  /**
   * Sets the convention to use
   * based on the query parameters
   */
  private async getConventionFlag() {
    const params = this.route.queryParams.subscribe(params => {
      let conventionId = params["convention"]
      if (conventionId) {
        this.setConvention(conventionId);
      }
    });
  }

  private async loadAbout() {
    let aboutUrl = this.getCurrentAssetPath() + "about.json";

    try {
      var conventionAbout;
      if (isDevMode() && this.currentConvention === 'dev') {
        conventionAbout = getSandboxConvention();
      } else {
        conventionAbout = await firstValueFrom(this.httpClient.get<ConventionAbout>(aboutUrl));
      }
      this.about.next(conventionAbout);

      await this.loadSchedule();
      await this.periodScheduleLoading();
    } catch (error) {
      console.error("Error loading about.json: " + JSON.stringify(error, null, 2));
    }
  }

  private updateInterval: any;
  private async periodScheduleLoading() {
    // Set interval to periodically check for updates
    this.updateInterval = setInterval(() => {
      this.loadSchedule();
    }, 120_000); // Check every two minute
  }

  private async loadSchedule() {
    const about = this.about.value;
    if (!about) {
      return;
    }

    if (isDevMode() && this.currentConvention === 'dev') {
      this.schedule.next(getSandboxSchedule());
      return;
    }

    var scheduleUrl = "schedule.json";
    if (about.schedule_path) {
      scheduleUrl = this.getCurrentAssetPath() + about.schedule_path;
    } else if (about.schedule_url) {
      scheduleUrl = about.schedule_url;
    }

    try {
      const response = await firstValueFrom(this.httpClient.get<ConventionSchedule>(scheduleUrl, {observe: 'response'}));
      const scheduleObject = response.body!;
      scheduleObject.lastUpdatedAt = response.headers.get('Last-Modified') ?? new Date().toISOString();
      this.schedule.next(scheduleObject);
    } catch (error) {
      console.error("Error loading schedule.json: " + JSON.stringify(error, null, 2));
      this.schedule.next(null);
    }
  }

  private setUrlToConvention(name: String) {
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: { "convention": name },
      queryParamsHandling: 'merge',
    });
  }

  public setConvention(name: string) {
    let conventions = this.config.conventions.filter((item) => item.name.includes(name));

    if (conventions.length > 0) {
      this.currentConvention = conventions[0].path;
      this.loadAbout();
      this.setUrlToConvention(name);
    } else {
      console.log("Convention " + name + " not found.");
    }
  }

  public useConventionTime(): boolean {
    const conventionAbout = this.about.getValue();
    if (conventionAbout) {
      return this.shouldUseConventionTime && conventionAbout.time_zone !== null;
    } else {
      return this.shouldUseConventionTime;
    }
  }

  public getCurrentAssetPath(): string {
    return "assets/events/" + this.currentConvention;
  }
}
