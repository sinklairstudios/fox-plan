import { TestBed } from '@angular/core/testing';

import { ConventionService } from './convention.service';

describe('PanelService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ConventionService = TestBed.inject(ConventionService);
    expect(service).toBeTruthy();
  });
});
