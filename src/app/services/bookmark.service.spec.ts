import { TestBed } from '@angular/core/testing';

import { BookmarkService } from './bookmark.service';

describe('BookmarkService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  var bookmarkspace = 'sample';

  it('should be created', () => {
    const service: BookmarkService = TestBed.inject(BookmarkService);
    expect(service).toBeTruthy();
  });

  it('should store entry', () => {
    const service: BookmarkService = TestBed.inject(BookmarkService);
    console.log(service.isBookmarked(bookmarkspace, 'furrytalentshow'));
    expect(service.isBookmarked(bookmarkspace, 'furrytalentshow')).toBe(false);
    service.addBookmark(bookmarkspace, 'furrytalentshow');
    expect(service.isBookmarked(bookmarkspace, 'furrytalentshow')).toBeTruthy();
    service.removeBookmark(bookmarkspace, 'furrytalentshow');
    expect(service.isBookmarked(bookmarkspace, 'furrytalentshow')).toBe(false);
  });
});
