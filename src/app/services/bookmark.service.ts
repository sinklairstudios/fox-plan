import { Injectable } from '@angular/core';

import { SharedBookmark } from '../objects/shared.bookmark';

@Injectable({
  providedIn: 'root'
})
export class BookmarkService {

  constructor() { }

  private getStorageKey(convention: string, panelName: string): string {
    return convention + ":" + panelName;
  }

  shared: SharedBookmark = {
    showOnlyBookmarked: false
  };

  isBookmarked(convention: string, panelName: string): boolean {
    if (typeof(Storage) !== "undefined") {
      let key = this.getStorageKey(convention, panelName);
      return !(localStorage.getItem(key) === null);
    }
    return false;
  }

  addBookmark(convention: string, panelName: string) {
    if (typeof(Storage) !== "undefined") {
      let key = this.getStorageKey(convention, panelName);
      try {
        localStorage.setItem(key, "");
      } catch (domException: any) {
        if (
          ['QuotaExceededError', 'NS_ERROR_DOM_QUOTA_REACHED'].includes(
            domException.name
          )
        ) {
          console.warn("Exceeded local storage limit. Cannot save bookmark entry " + key);
        }
      }
    }
  }

  removeBookmark(convention: string, panelName: string) {
    if (typeof(Storage) !== "undefined") {
      let key = this.getStorageKey(convention, panelName);
      localStorage.removeItem(key);
    }
  }

  /**
  Resources:
  - https://flaviocopes.com/web-storage-api/
  **/
}
