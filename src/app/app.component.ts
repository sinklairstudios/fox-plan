import { Component, inject } from '@angular/core';

import { Location } from '@angular/common';

import packageJson from '../../package.json';
import { ConventionService } from './services/convention.service';
import { BookmarkService } from './services/bookmark.service';
import { ConventionAbout } from './objects/convention';
import { NavigationbarComponent } from './navigationbar/navigationbar.component';
import { RouterOutlet } from '@angular/router';
import { NavigationtabsComponent } from './navigationtabs/navigationtabs.component';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
    imports: [NavigationbarComponent, RouterOutlet, NavigationtabsComponent]
})
export class AppComponent {
  private location = inject(Location);
  private bookmarkService = inject(BookmarkService);
  private conventionService = inject(ConventionService);

  title = 'FoxPlan';
  bookmarkStatus = this.bookmarkService.shared;
  public version: string = packageJson.version;
  public about: ConventionAbout | null = null;
  assetPath = "";

  constructor() {
    this.conventionService.about.subscribe((about: ConventionAbout | null) => {
      this.about = about
      this.assetPath = this.conventionService.getCurrentAssetPath();
    });
  }

  public isHidden() {
      var path = this.location.path();
      return path === "";
  }

  toggleShowBookmarkedPanels() {
    this.bookmarkStatus.showOnlyBookmarked = !this.bookmarkStatus.showOnlyBookmarked;
  }
}
