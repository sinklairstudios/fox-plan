import { Injectable, inject } from '@angular/core';
import { ConventionService } from './services/convention.service';
import { Panel } from './objects/panel';

@Injectable({
  providedIn: 'root'
})
export class PanelSharelinkGeneratorService {
  private conventionService = inject(ConventionService);


  convertStringToDate(stringDate: string): Date | null {
    try {
      return new Date(Date.parse(stringDate));
      // return moment(stringDate, "YYYY-MM-DDTHH:mm:ssZ").toDate();
    }
    catch (error) {
      console.error(error);
    }
    return null;
  }

  generateShareLink(panel: Panel) {
    const about = this.conventionService.about.value;
    const conventionName = about?.name ?? "the convention"
    const hashtag = (about) ? "#"+about.hashtag : "the convention"

    const panelPath = "/detail/" + panel.id;
    const queryParameters = window.location.search;
    const panelLink = window.location.protocol + '//' + window.location.host + panelPath + queryParameters;

    // https://simpletexting.com/how-to-create-a-link-that-sends-an-sms-text-message/
    // https://css-tricks.com/how-to-use-the-web-share-api/

    var startLocalString = "";
    var endLocalString = "";
    var postfix = "";
    if (about && this.conventionService.useConventionTime()) {
      startLocalString = this.convertStringToDate(panel.startDate)?.toLocaleString('en-US', { timeZone: about.time_zone_city }) ?? "n/a";
      endLocalString = this.convertStringToDate(panel.endDate)?.toLocaleString('en-US', { timeZone: about.time_zone_city }) ?? "n/a";
      postfix = "(local time at " + conventionName + ")";
    } else {
      startLocalString = this.convertStringToDate(panel.startDate)?.toLocaleString() ?? "n/a";
      endLocalString = this.convertStringToDate(panel.endDate)?.toLocaleString() ?? "n/a";
      postfix = "("+Intl.DateTimeFormat().resolvedOptions().timeZone+")";
    }

    let message = 'Hey, I\'m going to the panel \'' + panel.name + '\' at ' + hashtag + '\
     \nDescription: ' + panel.description + '\
     \n' + panel.location?.name + ' at ' + startLocalString + ' until ' +  endLocalString + ' ' + postfix;

    return {
      title: hashtag + ' - ' + panel.name,
      url: panelLink,
      text: message
    }
  }
}
