import { CommonModule } from '@angular/common';
import { Component, ElementRef, HostListener, Input, inject, input, output } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { MatListModule } from '@angular/material/list';
import { Panel } from '../objects/panel';
import { PanelSharelinkGeneratorService } from '../panel-sharelink-generator.service';

@Component({
    selector: 'app-context-menu',
    imports: [
        MatListModule,
        MatCardModule,
        CommonModule,
    ],
    templateUrl: './context-menu.component.html',
    styleUrl: './context-menu.component.css'
})
export class ContextMenuComponent {
  private panelShareLinkGenerator = inject(PanelSharelinkGeneratorService);
  private _eref = inject(ElementRef);

  @Input() show: boolean = false;
  readonly panel = input<Panel>();
  readonly closeMenu = output<void>();
  readonly posX = input<number>(0);
  readonly posY = input<number>(0);

  async getLink() {
    const panel = this.panel();
    if (!panel) { return };

    const linkObject = this.panelShareLinkGenerator.generateShareLink(panel);

    // copy to clipboard
    await navigator.clipboard.writeText(linkObject.url);
    this.show = false;
  }

  @HostListener('document:click', ['$event'])
  public onGlobalClick(event: any): void {
    if (!this._eref.nativeElement.contains(event.target)) {
      this.closeMenu.emit(); // Emit the event to signal the parent component
    }
  }
}
