import { Component, OnInit, inject } from '@angular/core';

import { FAQ } from '../objects/faq';

import { FAQService } from '../services/faq.service';
import { SearchService } from '../services/search.service';
import { MatNavList, MatListItem, MatListItemTitle, MatListItemLine, MatListItemMeta } from '@angular/material/list';

import { MatIcon } from '@angular/material/icon';
import { SearchPipe } from '../pipes/search';

@Component({
    selector: 'app-faq',
    templateUrl: './faq.component.html',
    styleUrls: ['./faq.component.css'],
    imports: [MatNavList, MatListItem, MatListItemTitle, MatListItemLine, MatListItemMeta, MatIcon, SearchPipe]
})
export class FaqComponent implements OnInit {
  private faqService = inject(FAQService);
  private searchService = inject(SearchService);

  faqs: FAQ[] = [];
  query: string = '';

  constructor() {
    // subscribe to FAQ updates
    this.faqService.faqs.subscribe(newFaqs => this.faqs = newFaqs);

    // subscribe to search field updates
    this.searchService.query$.subscribe(newQuery => this.query = newQuery);
  }

  ngOnInit() { }


  callurl(faq: FAQ) {
    if (faq.url) {
      open(faq.url);
    }
  }

}
