
type DateComparisonMode = 'conventionTime' | 'localTime'

/**
 * Check if two dates are the same day or not.
 * @param startDate 
 * @param endDate 
 * @param comparisonMode configure whether it should be compared on user local time or at convention time (default is convention time)
 * @returns 
 */
export function isSameDay(startDate: string, endDate: string, comparisonMode: DateComparisonMode = 'conventionTime'): boolean {
    const start = new Date(startDate)
    const end = new Date(endDate)
    
    if (comparisonMode === 'localTime') {
        return (
            start.getFullYear() === end.getFullYear() &&
            start.getMonth() === end.getMonth() &&
            start.getDate() === end.getDate()
        )
    } else {
        // Extract the substring from the start of the string to the 10th character (YYYY-MM-DD)
        const startDatePart = startDate.substring(0, 10);
        const endDatePart = endDate.substring(0, 10);

        // Compare the extracted parts
        return startDatePart === endDatePart;
    }
}
