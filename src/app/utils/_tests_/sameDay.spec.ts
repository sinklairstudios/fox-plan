import { isSameDay } from "../sameDay";

describe('isSameDay', () => {
    it('should return true for the same day in UTC', () => {
        expect(isSameDay('2025-01-04T10:00:00', '2025-01-04T22:00:00')).toBe(true);
    });

    it('should return false for two different days in UTC', () => {
        expect(isSameDay('2025-01-04T23:59:59', '2025-01-05T00:00:00')).toBe(false);
    });

    it('should return false for across midnight in time zone -07:00', () => {
        expect(isSameDay('2025-01-04T23:00:00-07:00', '2025-01-05T01:00:00-07:00')).toBe(false);
    });

    it('should return false for two different local days across midnight in time zone +03:00', () => {
        expect(isSameDay('2025-01-04T23:00:00+03:00', '2025-01-05T01:00:00+03:00')).toBe(false);
    });

    // out of scope for now
    // it('should handle different offsets correctly and return true for same day in +03:00', () => {
    //     expect(isSameDay('2025-01-04T15:00:00Z', '2025-01-05T01:00:00+03:00')).toBe(true);
    // });

    // it('should return false for two different days with varying time zones', () => {
    //     expect(isSameDay('2025-01-04T23:59:59-04:00', '2025-01-05T00:00:00-02:00')).toBe(true);
    // });

    it('should default to UTC if no offset is provided', () => {
        expect(isSameDay('2025-01-04T23:59:59', '2025-01-05T00:00:00')).toBe(false);
    });
});
