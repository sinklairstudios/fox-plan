/**
 * Deep comparison between two objects.
 * @param obj1 
 * @param obj2 
 * @returns 
 */
export function deepEqual(obj1: any, obj2: any) {
    if (obj1 === obj2) return true;
  
    if (typeof obj1 !== "object" || obj1 === null || typeof obj2 !== "object" || obj2 === null) {
      return false;
    }
  
    const keys1 = Object.keys(obj1);
    const keys2 = Object.keys(obj2);
  
    if (keys1.length !== keys2.length) return false;
  
    for (const key of keys1) {
      if (!keys2.includes(key) || !deepEqual(obj1[key], obj2[key])) {
        return false;
      }
    }
  
    return true;
  }
  
  /**
   * Deep comparision of two arrays.
   * 
   * The arrays will be sorted before running the comparisons.
   * @param arr1 
   * @param arr2 
   * @returns 
   */
  export function areArraysEqual(arr1: any[], arr2: any[]): boolean {
    if (arr1.length !== arr2.length) return false;
  
    const sortedArr1 = [...arr1].sort((a, b) => JSON.stringify(a).localeCompare(JSON.stringify(b)));
    const sortedArr2 = [...arr2].sort((a, b) => JSON.stringify(a).localeCompare(JSON.stringify(b)));
  
    return sortedArr1.every((item, index) => deepEqual(item, sortedArr2[index]));
  }
