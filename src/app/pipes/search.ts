import { Pipe, PipeTransform } from '@angular/core';
import { Searchable } from '../objects/searchable';

@Pipe({ name: 'search' })
export class SearchPipe<T extends Searchable> implements PipeTransform {
  public transform(list: T[], filterText: string): T[] {
    if (!filterText) return list;
    return (list || []).filter(item => item.searchterms?.toLowerCase().includes(filterText.toLowerCase()));
  }
}
