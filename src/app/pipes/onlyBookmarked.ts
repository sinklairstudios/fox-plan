import { Pipe, PipeTransform, inject } from '@angular/core';

import { BookmarkService } from '../services/bookmark.service';
import { SharedBookmark } from '../objects/shared.bookmark';
import { Bookmarkable } from '../objects/bookmarkable';

@Pipe({
    name: 'onlyBookmarkedIfActive',
    pure: false
})
export class MyOnlyBookmarkedPipe<T extends Bookmarkable> implements PipeTransform {
  private bookmarkService = inject(BookmarkService);

  bookmarkStatus: SharedBookmark;

  constructor() {
    this.bookmarkStatus = this.bookmarkService.shared;
  }

  transform(items: T[]): T[] {
    if (!items) {
      return items;
    }

    return items.filter(item => (item.isBookmarked === true || !this.bookmarkStatus.showOnlyBookmarked));
  }
}
