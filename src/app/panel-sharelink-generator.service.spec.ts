import { TestBed } from '@angular/core/testing';

import { PanelSharelinkGeneratorService } from './panel-sharelink-generator.service';

describe('PanelSharelinkGeneratorService', () => {
  let service: PanelSharelinkGeneratorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PanelSharelinkGeneratorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
