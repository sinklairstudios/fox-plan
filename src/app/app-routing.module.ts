import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';






const routes: Routes = [
  { path: '', loadComponent: () => import('./panels/panels.component').then(m => m.PanelsComponent) },
  { path: 'schedule/:id', loadComponent: () => import('./panels/panels.component').then(m => m.PanelsComponent) },
  { path: 'detail/:id', loadComponent: () => import('./panel-detail/panel-detail.component').then(m => m.PanelDetailComponent) },
  { path: 'map', loadComponent: () => import('./map/map.component').then(m => m.MapComponent) },
  { path: 'faq', loadComponent: () => import('./faq/faq.component').then(m => m.FaqComponent) },
  { path: 'settings', loadComponent: () => import('./settings/settings.component').then(m => m.SettingsComponent) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    // onSameUrlNavigation: 'reload',
    scrollPositionRestoration: 'enabled',
    anchorScrolling: 'enabled',
    scrollOffset: [0, 120],
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
