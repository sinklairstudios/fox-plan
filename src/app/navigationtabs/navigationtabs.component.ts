import { Component, OnInit, inject } from '@angular/core';
import { Location } from '@angular/common';

import { SearchService } from '../services/search.service';
import { FAQService } from '../services/faq.service';
import { FAQ } from '../objects/faq';
import { ConventionService } from '../services/convention.service';
import { PanelService } from '../services/panel.service';
import { Panel } from '../objects/panel';
import { ConventionAbout } from '../objects/convention';
import { MatToolbar } from '@angular/material/toolbar';
import { MatFormField, MatSuffix } from '@angular/material/form-field';
import { MatInput } from '@angular/material/input';
import { FormsModule } from '@angular/forms';
import { MatButton } from '@angular/material/button';
import { MatTabNav, MatTabLink, MatTabNavPanel } from '@angular/material/tabs';
import { RouterLink } from '@angular/router';
import { MatIcon } from '@angular/material/icon';

@Component({
    selector: 'app-navigationtabs',
    templateUrl: './navigationtabs.component.html',
    styleUrls: ['./navigationtabs.component.css'],
    imports: [MatToolbar, MatFormField, MatInput, FormsModule, MatButton, MatSuffix, MatTabNav, MatTabLink, RouterLink, MatIcon, MatTabNavPanel]
})
export class NavigationtabsComponent implements OnInit {
  private conventionService = inject(ConventionService);
  private faqService = inject(FAQService);
  private location = inject(Location);
  private panelService = inject(PanelService);
  searchService = inject(SearchService);

  hasFAQ = false;
  hasMaps = false;
  hasSchedule = false;

  constructor() {
      const conventionService = this.conventionService;
      const faqService = this.faqService;
      const panelService = this.panelService;

      conventionService.about.subscribe((about: ConventionAbout | null) => {
        this.hasMaps = (about?.maps?.length ?? 0) > 0;
      });

      faqService.faqs.subscribe((data: FAQ[]) => {
        this.hasFAQ = data.length > 0;
      });

      panelService.panels.subscribe((panels: Panel[]) => {
        console.log("panels - schedule")
        this.hasSchedule = panels.length > 0;
      })
  }

  ngOnInit() { }

  public getCurrentPage(): string {
    var path = this.location.path();

    if (path.startsWith("/map")) {
      return "map";
    }

    if (path.startsWith("/faq")) {
      return "faq";
    }

    if (path.startsWith("/detail")) {
      return "detail";
    }

    if (path.startsWith("/settings")) {
      return "settings";
    }

    return "schedule";
  }

  public hasSearch(): boolean {
    let currentPage = this.getCurrentPage();
    switch (currentPage) {
      case "schedule":
        return true;
      case "faq":
        return true;
      default:
        return false;
    }
  }

  public clearSearch() {
    this.searchService.query = "";
  }

}
