import { Component, OnInit, inject } from '@angular/core';

import { Location } from '@angular/common';

import { BookmarkService } from '../services/bookmark.service';
import { ConventionService } from '../services/convention.service';
import { MatToolbar, MatToolbarRow } from '@angular/material/toolbar';
import { RouterLink } from '@angular/router';
import { MatButton } from '@angular/material/button';
import { MatIcon } from '@angular/material/icon';

@Component({
    selector: 'app-navigationbar',
    templateUrl: './navigationbar.component.html',
    styleUrls: ['./navigationbar.component.css'],
    imports: [MatToolbar, MatToolbarRow, RouterLink, MatButton, MatIcon]
})
export class NavigationbarComponent implements OnInit {
  private location = inject(Location);
  private bookmarkService = inject(BookmarkService);
  private conventionService = inject(ConventionService);

  bookmarkStatus = this.bookmarkService.shared;
  about: any = {};

  constructor() {
    this.conventionService.about.subscribe((value: any) => this.about = value );
  }

  ngOnInit() { }

  public isHidden() {
      var path = this.location.path();
      // if root or if root that only has query parameters (?) or under /schedule
      return path === "" || path.includes("schedule") || path.indexOf("?") === 0;
  }

  toggleShowBookmarkedPanels() {
    this.bookmarkStatus.showOnlyBookmarked = !this.bookmarkStatus.showOnlyBookmarked;
  }
}
