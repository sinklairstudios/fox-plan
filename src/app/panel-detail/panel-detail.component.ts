import { Component, OnInit, Input, inject } from '@angular/core';
import { ActivatedRoute, RouterLink } from '@angular/router';
import { Location, NgIf, NgClass, NgFor, UpperCasePipe, DatePipe } from '@angular/common';

import { Panel } from '../objects/panel';

import { PanelService } from '../services/panel.service';
import { BookmarkService } from '../services/bookmark.service';
import { ConventionService } from '../services/convention.service';
import { PanelSharelinkGeneratorService } from '../panel-sharelink-generator.service';
import { MatCard, MatCardHeader, MatCardTitle, MatCardSubtitle, MatCardContent, MatCardActions } from '@angular/material/card';
import { MatChipOption } from '@angular/material/chips';
import { MatAnchor, MatButton } from '@angular/material/button';
import { isSameDay } from '../utils/sameDay';

@Component({
    selector: 'app-panel-detail',
    templateUrl: './panel-detail.component.html',
    styleUrls: ['./panel-detail.component.css'],
    imports: [NgIf, MatCard, NgClass, MatCardHeader, MatCardTitle, MatCardSubtitle, MatChipOption, MatCardContent, NgFor, MatAnchor, MatCardActions, MatButton, RouterLink, UpperCasePipe, DatePipe]
})
export class PanelDetailComponent implements OnInit {
  private route = inject(ActivatedRoute);
  private panelService = inject(PanelService);
  private location = inject(Location);
  private bookmarkService = inject(BookmarkService);
  private conventionService = inject(ConventionService);
  private panelShareLinkGenerator = inject(PanelSharelinkGeneratorService);

  panel: any = {};
  isBookmarked: boolean = false;
  supportsSharing: boolean = false;
  about: any = {};
  panels: Panel[] = [];
  useConventionTime?: boolean;
  isSameDay = isSameDay

  constructor() {
    this.panelService.panels.subscribe(newPanels => {
      this.panels = newPanels
      this.getPanel();
    });

    this.conventionService.about.subscribe(newAbout => {
      this.about = newAbout
      this.useConventionTime = this.conventionService.useConventionTime();
    });
   }

  ngOnInit() {
    let windownavigator = (window.navigator as any);
    this.supportsSharing = windownavigator.share !== undefined;
  }

  getPanel(): void {
    const id: string = this.route.snapshot.paramMap.get('id') || "";
    this.panel = this.panels.find(panel => panel.id === id);
  }

  share(): void {
    //sms://&body="Hello, I'm goint going to the following panel at FC"
    // $windows.open('sms://&body=Hello World', '_blank');
    let windownavigator = (window.navigator as any);
    let linkObject = this.panelShareLinkGenerator.generateShareLink(this.panel);

    if (windownavigator.share) {
      windownavigator.share(linkObject).then(() => {
        console.log('Thanks for sharing!');
      })
      .catch(console.error);
    } else {
      // fallback
    }
  }

  toggleBookmark() {
    if (this.panel.isBookmarked) {
      this.bookmarkService.removeBookmark(this.about.name, this.panel.id);
    } else {
      this.bookmarkService.addBookmark(this.about.name, this.panel.id);
    }
    this.panel.isBookmarked = this.bookmarkService.isBookmarked(this.about.name, this.panel.id);
  }

  isUrl(possibleUrlString?: string) {
    var expression = /^(http|https):\/\/[^ "]+$/;
    var regex = new RegExp(expression);
    return regex.test(possibleUrlString || "");
  }
}
