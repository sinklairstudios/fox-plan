import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PanelDetailComponent } from './panel-detail.component';

import { RouterTestingModule } from "@angular/router/testing";
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';

describe('PanelDetailComponent', () => {
  let component: PanelDetailComponent;
  let fixture: ComponentFixture<PanelDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
    imports: [
        RouterTestingModule,
        MatCardModule,
        MatButtonModule,
        PanelDetailComponent
    ]
})
    .compileComponents();

    fixture = TestBed.createComponent(PanelDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    // FIXME: detail view needs :id input from router in order to successfully be tested
    expect(component).toBeTruthy();
  });
});
