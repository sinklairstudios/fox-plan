import { Component, OnInit, inject } from '@angular/core';

import { ConventionService } from '../services/convention.service';
import { ConventionAbout, MapItem } from '../objects/convention';

import { MatCard, MatCardHeader, MatCardTitle, MatCardContent, MatCardActions } from '@angular/material/card';
import { MatAnchor } from '@angular/material/button';

@Component({
    selector: 'app-map',
    templateUrl: './map.component.html',
    styleUrls: ['./map.component.css'],
    imports: [MatCard, MatCardHeader, MatCardTitle, MatCardContent, MatCardActions, MatAnchor]
})
export class MapComponent implements OnInit {
  private conventionService = inject(ConventionService);

  assetPath = "";
  mapItems: MapItem[] = [];

  constructor() {
    this.conventionService.about.subscribe((about: ConventionAbout | null) => {
      this.mapItems = about?.maps ?? [];
      this.assetPath =this.conventionService.getCurrentAssetPath();
    });
  }

  ngOnInit() {
  }

}
