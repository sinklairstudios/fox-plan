import { Searchable } from './searchable';

export class FAQ implements Searchable {
  name: string = "";
  description: string = "";
  url?: string;
  searchterms?: string = "";
}
