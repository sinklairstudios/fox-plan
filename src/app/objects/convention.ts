import { FAQ } from "./faq"


export class ConventionStyle {
  "logo_name": string
  "background_image_name": string
  "background_color": string

  "primary_font": string
  "primary_font_color": string
  "primary_color": string

  "secondary_font_color": string
  "secondary_color": string
}

export class MapItem {
  "name": string
  "image_path": string
  "pdf": string
}

export class ConventionAbout {
  "name": string
  "title": string
  "hashtag": string
  "version": number
  "schedule_path"?: string
  "schedule_url"?: string
  "time_zone": string
  "time_zone_city": string
  "style": ConventionStyle
  "faqs": FAQ[]
  "maps": MapItem[]
}
