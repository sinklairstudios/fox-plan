import { Searchable } from './searchable';

export class ConventionSchedule {
  "name": string
  "entries": Panel[]

  // COMPUTED VALUES (do not provide via API)
  // TODO: externalize these
  "lastUpdatedAt"?: string 
}

export class Panel implements Searchable {
  id: string = "";
  name: string = "";
  startDate: string = "";
  endDate: string = "";
  description: string = "";
  presenters: Presenter[] = [];
  type: string = "";
  maturity?: string = "";
  location?: Room;

  // COMPUTED VALUES (do not provide via API)
  // TODO: externalize these
  isBookmarked?: boolean = false;
  searchterms?: string = "";
}

export class Presenter {
  name: string = "";
  description?: string;
  social?: Social[];
}

export class Social {
  type?: string;
  url?: string;
  name?: string;
}

export class Room {
  name: string = "";
  number?: string;
  floor?: string;
  building?: string;

  public description(): String {
    var description = "";
    if (this.building) {
      description += "Building: " + this.building + " - ";
    }
    description += "Room: " + this.name;
    return description;
  }
}
