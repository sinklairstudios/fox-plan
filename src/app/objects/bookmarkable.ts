export interface Bookmarkable {
  isBookmarked: boolean;
}
