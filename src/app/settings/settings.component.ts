import { Component, inject } from '@angular/core';
import { ConventionService } from '../services/convention.service';
import { MatCard, MatCardHeader, MatCardTitle, MatCardSubtitle, MatCardContent } from '@angular/material/card';
import { MatNavList, MatListItem, MatListItemTitle } from '@angular/material/list';
import { NgClass } from '@angular/common';
import { MatButton } from '@angular/material/button';

@Component({
    selector: 'app-settings',
    templateUrl: './settings.component.html',
    styleUrls: ['./settings.component.css'],
    imports: [MatCard, MatCardHeader, MatCardTitle, MatCardSubtitle, MatCardContent, MatNavList, MatListItem, NgClass, MatListItemTitle, MatButton]
})
export class SettingsComponent {
  private conventionService = inject(ConventionService);


  about: any = {};
  conventions: any[] = [];
  currentConvention: string = '';

  constructor() {
    this.conventionService.about.subscribe((value: any) => {
      this.about = value
      this.currentConvention = this.conventionService.currentConvention;
      this.conventions = this.conventionService.config.conventions;
    });
  }

  onSelect(conventionPath: string) {
    this.conventionService.setConvention(conventionPath);
  }

  forceReload() {
    console.log("heellooo")
    if ('serviceWorker' in navigator) {
      navigator.serviceWorker.getRegistrations().then((registrations) => {
        // Unregister all service workers
        for (let registration of registrations) {
          registration.unregister();
        }
        // Clear caches
        caches.keys().then((cacheNames) => {
          cacheNames.forEach((cacheName) => {
            caches.delete(cacheName);
          });
        }).then(() => {
          // Reload the page
          window.location.reload();
        });
      });
    } else {
      // Just reload if service worker is not supported
      window.location.reload();
    }
  }
}
