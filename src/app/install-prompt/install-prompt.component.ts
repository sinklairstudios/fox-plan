import { Component, OnInit } from '@angular/core';

import { MatCard, MatCardHeader, MatCardTitle, MatCardContent, MatCardActions } from '@angular/material/card';
import { MatIcon } from '@angular/material/icon';
import { MatAnchor } from '@angular/material/button';

@Component({
    selector: 'app-install-prompt',
    templateUrl: './install-prompt.component.html',
    styleUrls: ['./install-prompt.component.scss'],
    imports: [MatCard, MatCardHeader, MatCardTitle, MatCardContent, MatIcon, MatCardActions, MatAnchor]
})
export class InstallPromptComponent implements OnInit {
  hideInstallPrompt: string = sessionStorage.getItem('hideInstallationPrompt') || '';
  offerInstalling: boolean = !window.matchMedia('(display-mode: standalone)').matches && /iPad|iPhone|iPod|Android/.test(navigator.userAgent);
  isAndroid: boolean = /Android/.test(navigator.userAgent);
  installing: boolean = false;


  constructor() { }

  ngOnInit() {
  }

  dismiss() {
    if (this.installing) {
      // dismissing after trying to install will not save to the session
      // storage, next time when opening the schedule, the installation
      // prompt will re-appear immediately
      this.installing = false;
    } else {
      sessionStorage.setItem('hideInstallationPrompt', 'true');
      this.hideInstallPrompt = 'true';
    }
    this.offerInstalling = false;
  }

  install() {
    this.installing = true;
  }
}
